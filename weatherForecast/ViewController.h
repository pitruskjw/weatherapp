//
//  ViewController.h
//  weatherForecast
//
//  Created by User on 01.05.2017.
//  Copyright © 2017 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *CityLabel;
@property (weak, nonatomic) IBOutlet UILabel *TemperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *HumidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *PressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *WeatherDescLabel;

@property (weak, nonatomic) IBOutlet UIImageView *WeatherIconImage;

@property NSString* cityName;

- (IBAction)search:(UIButton *)sender;

@end

