//
//  ViewController.m
//  weatherForecast
//
//  Created by User on 01.05.2017.
//  Copyright © 2017 User. All rights reserved.
//

#import "ViewController.h"
#import "WeatherRequest.h"
#import "Enums.m"

@interface ViewController ()

@end

@implementation ViewController
{
    WeatherRequest *wr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _CityLabel.text = @"Welcome";
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"TestNotification"
                                               object:nil];

    wr = [[WeatherRequest alloc]initWithCity:_cityName];
    [wr getWeatherDataForCity];
    
    NSLog(@"%@", _cityName);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)search:(UIButton *)sender {
    [wr getWeatherDataForCity];
}

-(void)receiveTestNotification:(NSNotification *) notification
{
    //http://stackoverflow.com/a/15948789
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *dictionary = [wr getAPICallResult];
    
        _CityLabel.text = [dictionary valueForKey:@"City"];
        
        NSInteger temperatureUnits = [[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"];
        if(temperatureUnits == CELSJUS)
            _TemperatureLabel.text = [[self convertToCelsjuszFromKelvin:[[dictionary valueForKey:@"Temperature"] stringValue]]stringByAppendingString:@" *C"];
        else
            _TemperatureLabel.text = [[[dictionary valueForKey:@"Temperature"] stringValue]stringByAppendingString:@" K"];
        _HumidityLabel.text = [[[dictionary valueForKey:@"Humidity"] stringValue] stringByAppendingString:@"%"];
        _PressureLabel.text = [[[dictionary valueForKey:@"Pressure"] stringValue] stringByAppendingString:@"hPa"];
        _WeatherDescLabel.text = [dictionary valueForKey:@"WeatherDescription"][0];
        NSString* iconName = [dictionary valueForKey:@"WeatherIcon"][0];
        _WeatherIconImage.image = [UIImage imageNamed:iconName];

    });
    
}

-(NSString *)convertToCelsjuszFromKelvin:(NSString *)kelvin
{
    int celsjus = [kelvin intValue]-273;
    return [NSString stringWithFormat:@"%d",celsjus];
}

@end
