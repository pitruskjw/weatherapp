//
//  WeatherRequest.m
//  weatherForecast
//
//  Created by User on 02.05.2017.
//  Copyright © 2017 User. All rights reserved.
//

#import "WeatherRequest.h"
#import "ViewController.h"

@implementation WeatherRequest
{
    NSString *_city;
    NSNumber *_temp;
    NSNumber *_humidity;
    NSNumber *_pressure;
    NSString *_weatherDescription;
    NSString *_weatherIcon;
    NSDictionary *_weatherRequestResult;

}

-(id)initWithCity:(NSString*)city
{
    self = [super init];
    if(self)
    {
        self->_city = city;
    }
    return self;
}


-(void)getWeatherDataForCity
{
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@&APPID=81a505ec8596facd1ab0ebac92dd530e",_city];
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    NSString *escapedPath = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [request setURL:[NSURL URLWithString:escapedPath]];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask *sesionTask = [[NSURLSession sharedSession]dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response,NSError *error){
        if(error == nil)
        {
            NSError *e = nil;
            NSJSONSerialization *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableLeaves error: &e];
            
            if (!jsonArray) {
                NSLog(@"Error parsing JSON: %@", e);
            } else {
                NSNumberFormatter *formater = [[NSNumberFormatter alloc]init];
                formater.numberStyle = NSNumberFormatterDecimalStyle;
     
       
                
                _temp = [NSNumber numberWithFloat:[[[jsonArray valueForKey:@"main"]valueForKey:@"temp"] floatValue]];
                _humidity = [NSNumber numberWithFloat:[[[jsonArray valueForKey:@"main"]valueForKey:@"humidity"]floatValue]];
                _pressure = [NSNumber numberWithFloat:[[[jsonArray valueForKey:@"main"]valueForKey:@"pressure"]floatValue]];
                _weatherDescription = [[jsonArray valueForKey:@"weather"]valueForKey:@"description"];
                _weatherIcon = [[jsonArray valueForKey:@"weather"]valueForKey:@"icon"];
//                NSLog(@"%@",[[jsonArray valueForKey:@"main"]valueForKey:@"temp"]);
                _weatherRequestResult = @{
                    @"City":_city,
                    @"Temperature":_temp,
                    @"Humidity":_humidity,
                    @"Pressure":_pressure,
                    @"WeatherDescription":_weatherDescription,
                    @"WeatherIcon":_weatherIcon
                };
                
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"TestNotification"
                 object:self];
                
            }
        }
        else
        {
            NSLog(@"Error!!!");
        }
 
    }];
    
    [sesionTask resume];
    
}

-(NSDictionary*)getAPICallResult
{
    return _weatherRequestResult;
}

@end

