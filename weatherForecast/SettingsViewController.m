//
//  SettingsViewController.m
//  weatherForecast
//
//  Created by User on 11.06.2017.
//  Copyright © 2017 User. All rights reserved.
//

#import "SettingsViewController.h"
#import "Enums.m"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.    
    
    NSInteger temperatureUnits = [[NSUserDefaults standardUserDefaults] integerForKey:@"temperatureUnit"];
    _temperaturUnitSegment.selectedSegmentIndex = temperatureUnits;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)temperatureunitsChanged:(UISegmentedControl *)sender {
    NSInteger temperatureUnit = [_temperaturUnitSegment selectedSegmentIndex];
    [[NSUserDefaults standardUserDefaults] setInteger:temperatureUnit forKey:@"temperatureUnit"];
}
@end
