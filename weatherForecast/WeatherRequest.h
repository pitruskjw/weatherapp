//
//  WeatherRequest.h
//  weatherForecast
//
//  Created by User on 02.05.2017.
//  Copyright © 2017 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherRequest : NSObject



-(id)initWithCity:(NSString*)city;
-(void)getWeatherDataForCity;
-(NSDictionary*)getAPICallResult;

@end
