//
//  SettingsViewController.h
//  weatherForecast
//
//  Created by User on 11.06.2017.
//  Copyright © 2017 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

- (IBAction)temperatureunitsChanged:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *temperaturUnitSegment;

@end
